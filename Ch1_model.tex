\chapter{\textsl{Ex vivo} Model}
\label{Ch1_model}
\addcontentsline{lof}{chapter}{Chapter \ref{Ch1_model}: \textsl{Ex vivo} model}
\addcontentsline{lot}{chapter}{Chapter \ref{Ch1_model}: \textsl{Ex vivo} model}
\section{Introduction}
This chapter describes the development of an organotypic tissue culture model for studying cochlear hair cell responses to ototoxic stress. This model was used to investigate the role of extracellular nucleotides and nucleosides in the maintenance of the sensory hair cell population in the presence of the ototoxic aminoglycoside antibiotic neomycin.\\[-1em]

Organotypic cultures of the cochlea (the organ of Corti explants) are useful for the study of normal cytoarchitecture of the organ of Corti in culture conditions. They can also be used to study the effects of ototoxic and neuroprotective drugs on the cochlea, as the response can be easily quantified by hair cell counting. A disadvantage of this method is that the organ of Corti explants are difficult to maintain for an extended period of time, and successfully culturing a mature cochlea for more than 24 hours has not been established \citep{malgrange2002identification}. \\[-1em]

While human and most mammals are born with functional hearing at birth, the mouse cochlea undergoes numerous changes during the early postnatal period and does not fully mature until P21. Immature inner hair cells (IHC) and outer hair cells (OHC) in the mouse cochlea are present at birth. Hair cell number can increase over the first four days after birth and the elongation of the organ of Corti to the adult length involves steady growth of IHC and OHC in size up to P16 \citep{morsli1998development}. The surface morphology (apical circumference) of OHC resembles the adult at P5 and P7, as they display the `V' configuration of the OHC hair bundle \citep{etournay2010cochlear}. However, OHC, particularly at the basal turn, do not fully mature until P12 \citep{etournay2010cochlear}. Transduction currents appear in the basal turn at birth, but does not progress to the middle and apical turns until P2 and P6 respectively, indicating a tonotopic gradient in sensory transduction development \citep{lelli2009tonotopic}. The innervation of IHC and OHC at birth also under go  retraction and synaptic pruning during the first postnatal week \citep{huang2007spatiotemporal}. \\[-1em]

Even though it would be desirable to culture the mature organ of Corti the feasibility of this approach is governed by both dissection techniques and the vulnerability of the sensory hair cells in cell culture conditions. This chapter describes the dissection techniques used in animals at various developmental stages (P3, P7 and P12) and compares the cell culture survival rate to determine the best approach for the organotypic tissue culture model.   \\[-1em]

\section{Material and Methods}
\subsection{Chemicals and Solutions}
\subsubsection{Dissection solution}
After removing the cochlea, the sensory epithelium was micro-dissected in ice-cold dissection solution, which represents a modified artificial perilymph with the following composition: NaCl, 154 mM; KCl, 5.8 mM; HEPES, 10 mM; NaH\textsubscript{2}PO\textsubscript{4}; 0.7 mM; D-glucose, 10 mM; MgCl\textsubscript{2}.6H\textsubscript{2}O, 0.9 mM; sodium pyruvate, 2 mM; MEM amino acids, 10 mM; MEM vitamins, 1\% v/v and CaCl\textsubscript{2}.2H\textsubscript{2}, 1.3 mM.  \\[-1em] 

\subsubsection{Collagen gel}
Rat tail collagen (Thermo Fisher, MA USA) was used to adhere the organ of Corti explant to the coverslip and thus provided structural support for the tissues. The collagen matrix contained rat tail collagen (2.4 $mg/mL$) with the following composition: 80\% v/v of collagen stock (3 $mg/mL$), 10\% v/v of culture media, and 2.5\% v/v of 1$M$ NaOH in miliQ water. The collagen gel forms at room temperature, hence the collagen mixture was kept on ice until ready for use. A drop of the collagen mixture (15 - 20 $\mu L$) was transferred and evenly spread onto a sterile 13 mm round coverslip. The coverslip was then incubated for 45 minutes at 37$^{\circ}$C or until the gel had set.  \\[-1.5em]
\subsubsection{Normal culture medium}
The normal culture medium is designed to support the growth of mammalian cells in culture. Dulbecco's Modified Eagle Medium (DMEM, Thermo Fisher) and Earles Balanced Salt Solution (EBSS, Thermo Fisher) was used in a 2:1 ratio with addition of 5\% of fetal bovine serum (FBS, Gibco Invitrogen) and 30 units/mL of penicillin G (Sigma Aldrich).  \\[-1.5em]

\subsubsection{Neomycin}
Neomycin is an aminoglycoside antibiotic toxic to the inner ear, and cochlear hair cells in particular \citep{forge2000aminoglycoside}. It is commonly used to study ototoxicity \textsl{ex vivo} \citep{lowenheim1999determination, vintila2009neomycin, wang2003peptide}. Neomycin was purchased in the powder form from Sigma Aldrich and prepared as a 10 mM stock solution. It was further diluted in normal culture medium to 1 mM for use in this study. \\[-1.5em]

\subsection{Cochlear Dissection and Maintenance}
To gain access to the delicate sensory hair cells in the inner ear, the cochlea must first be removed from the auditory bulla (gross dissection) followed by micro-dissection of the sensory epithelium.  \\[-1.5em]

\subsubsection{Gross dissection}
C57BL/6 mouse pup (P3 -- P7) was decapitated at the base of the foramen magnum and the head was briefly rinsed in 70\% ethanol. The cranium was opened along the sagittal suture and the forebrain and cerebellum removed. The auditory bulla from each ear was removed along with the temporal bones. The temporal bones were then transferred into the dissection solution. Each cochlea was carefully extracted from the auditory bulla by tracing the outline of the bony labyrinth. The extracted cochlea was then prepared for micro-dissection by removing soft tissues attached to the otic capsule.  \\[-1.5em]

\subsubsection{Micro-dissection}
Micro-dissection of the sensory epithelium of the neonatal mouse cochlea (P3, P7 and P12) was carried out in ice-cold modified artificial perilymph solution. Minor differences exist between different dissection protocols \citep{parker2010primary, sobkowicz1993tissue}, but a successful dissection of the sensory epithelium is achieved by removing the bony labyrinth, spiral ligament and the tectorial membrane without damaging the sensory hair cells. In this study, the auditory bulla was removed, exposing the otic capsule, which was then decapsulated slowly from the apex to the base. This leaves the organ of Corti and the attached spiral ligament coiled around the modiolus. The spiral ligament was gently removed from the sensory epithelium by pulling in opposite directions as shown in the video by \citet{parker2010primary}. This method worked well for very young animals, but the spiral ligament from a more mature cochlea needed to be removed one small piece at a time to avoid detaching the sensory epithelium, particularly in the basal turn. \\[-1em]

In this study, a slightly different approach from \cite{parker2010primary} was adapted to dissect the sensory epithelium. Once the otic capsule was removed, the sensory epithelium was exposed by partially removing the spiral ligament at the dotted red line shown in Fig. \ref{diag:dissection} and mounted onto the coverslip (Fig. \ref{diag:dissection_flatten}). The tectorial membrane was gently removed, but the Reissner's membrane was flattened and kept away from the Organ of Corti (Fig. \ref{diag:dissection_flatten}). Depending on the age of the animal, the dissection procedure was slightly altered. Age-specific dissection procedures are described in the following sections.  \\[-1.5em]

\paragraph{P14 and P12 explants} \mbox{} \\
Dissection of the cochlea from postnatal days 12 and 14 presents a challenge as the bone has significantly ossified by this age, and removing the bony labyrinth often brings the spiral ligament along, which may result in damage to the sensory epithelium or separate it from the spiral limbus. Therefore, extreme care must be taken to remove the bony labyrinth without damaging the delicate sensory epithelium underneath. The spiral ligament at this age must be removed with a pair of scissors or forceps and processed piece by piece in a time consuming manner. \\[-1.5em]

\begin{figure}[p]
	\centering
	\includegraphics[width=0.9\textwidth]{diag_dissection1}	
	\caption[Schematic diagram of a cross section of the organ of Corti]{It shows a cross section of the cochlear tissues including the bony labyrinth, spiral ligament, the sensory epithelium and the spiral limbus. The dotted red line indicates the point for partial spiral ligament removal.}
	\label{diag:dissection}%
\end{figure}

\begin{figure} [p]
	\centering
	\includegraphics[width=1\textwidth]{diag_dissection_flattern}	
	\caption[Schematic diagram of a flattened cross section of the organ of Corti.]{This schematic of a cross section of the organ of Corti with the spiral ligament and the Reissner's membrane (tectorial membrane removed) pushed away from the sensory epithelium and flattened onto the collagen (light blue) coated coverslip. Spiral ligament had been partially removed at the dotted line shown in figure \ref{diag:dissection}. Red arrows show the direction of pressure on the Reissner's membrane and spiral ligament.}%
	\label{diag:dissection_flatten}%
\end{figure}

\paragraph{P7 explants} \mbox{} \\
At postnatal day 7, the cochlear bony labyrinth is partly calcified, but still relatively soft which makes it easy to chip and peel – similar to peeling the eggshell of a boiled egg. The bony labyrinth is most easily removed at this age and does not stick to the spiral ligament like the P12 -- 14 cochlea, which makes it very easy to dissect. However, care must be taken when removing the spiral ligament from the sensory epithelium particularly in the basal turn, where pulling off could also result in the detachment of the sensory epithelium from the spiral limbus along with the lateral wall structures. Therefore, the method of peeling off half of the spiral ligament and flattening the spiral ligament on to the dissection plate works best at this age (Fig. \ref{diag:dissection} and \ref{diag:dissection_flatten}). The middle and apical regions of the spiral ligament may be pulled off without risking damage to the sensory hair cells. \\[-1.5em]

\paragraph{P3 explants} \mbox{} \\
A postnatal day 3 cochlea is not yet calcified and the otic capsule can be peeled off without affecting the spiral ligament. Separating the spiral ligament from the sensory epithelium in P3 cochlea is straightforward, as the sensory epithelium can be held with one forceps and the spiral ligament with another at the basal turn and then pulled in opposite directions \citep{parker2010primary}. However, in this study, the dissection method described in Fig. \ref{diag:dissection} and \ref{diag:dissection_flatten} was followed as it provides greatest preservation to the sensory epithelium. \\[-1.5em]

\subsubsection{Maintaining the Organ of Corti Explants}
Following micro-dissection, the sensory epithelium was transferred onto the collagen coated coverslip. This was done by submerging the collagen coated coverslip in the dissection solution and gently scooping up the cochlear explant. The coverslip and cochlear explant were immediately transferred to a single well within the 24-well plate (explant side up) and 200$\mu$L of normal culture medium was added. This volume submerges the cochlear explant and provides meniscus pressure to keep the explant adhered to the coverslip.  \\[-1.5em]
%The organ of Corti explant was positioned in the correct orientation, and 
\subsection{Tissue Processing}
\subsubsection{Phalloidin staining}
Hair cell survival in organotypic tissue culture was investigated using Phalloidin staining of the actin filaments in the cuticular plates and stereocilia of the sensory hair cells. Cochlear explants were fixed after 42 hours of incubation with 4\% paraformaldehyde (PFA) for 20 minutes at room temperature. The explants were then washed in 0.1M of PBS, permeabilised with 1\% of Triton-X 100 for 1 hour at room temperature followed by another wash with 0.1M of PBS. The organ of Corti was stained with Alexa-488 phalloidin (1:600 dilution) for 1 hour at room temperature protected from light. At the end of incubation, the cells were washed and mounted with Citiflour onto a microscope slide (Fig. \ref{diag:mountingslide}). \\[-1.5em]

\subsubsection{Mounting tissue onto slides}
To mount the cochlear tissue with the coverslip onto a microscope slide, the thickness of the coverslip and tissue explant was corrected for by using 2 layers of Scotch Magic tape and a `square' carved into it (Fig. \ref{diag:mountingslide}). The 13 mm round coverslip with the explant was then placed in the middle of the square and submerged in antifade mounting medium (Citiflour) as shown in Fig. \ref{diag:mountingslide}. The explant was covered with another coverslip and sealed using nail polish.  Images were captured using Eclipse 80i Nikon microscope (Japan) using the 20x objective at the highest resolution (2560 x 1920 pixels).  \\[-1.5em]

\begin{figure}%
	\centering
	\includegraphics[width=0.6\textwidth]{mounting_slide}	
	\caption[Schematic diagram of cochlear explant mounted onto a microscope slide]{A schematic diagram showing a cochlear explant mounted onto a microscope slide. The coverslip and explant sit in a `carved-square' made of scotch tape, submerged in antifade mounting medium (Citiflour) and coverslipped and sealed with nail polish. }
	\label{diag:mountingslide}%
\end{figure}


\section{Results}
%\subsection{Structural Integrity of the Organ of Corti}
\subsection{Dissection damage}
The integrity of the organ of Corti explants in cell culture was assessed at different ages. To discriminate dissection damage from tissue culture damage, cochlear explants were dissected and immediately fixed with 4\% PFA and actin filaments stained with phalloidin-488. Figure \ref{fig:img_0hrs_control} shows the typical organ of Corti at P5, P7 and P12 immediately following dissection. Micro-dissection of the sensory epithelium was typically completed within a short timeframe (15 mins), which is crucial for minimising tissue damage. Using the dissection technique described above, very little mechanical damage was observed in the P5 and P7 cochlea and the sensory epithelium retained its organised architecture (Fig. \ref{fig:img_0hrs_control} A -- B). In a P12 cochlea, the micro-dissection of the sensory epithelium often resulted in partial or complete detachment of the sensory hair cells from the spiral limbus (Fig. \ref{fig:img_0hrs_control} C and Fig. \ref{fig:img_missing_epithelium}). \\[-1.5em]

\begin{figure}
	\centering
	\textbf{Dissection images of neonatal cochlea}\\[0.3cm]
	\begin{tabular}{ccc}
		\textbf{P5} & \textbf{P7} &  \textbf{P12}\\
		\includegraphics[width=0.3\textwidth]{P5_0hrs_apical.jpg} & \includegraphics[width=0.3\textwidth]{P7_0hrs_apical.jpg} & \includegraphics[width=0.3\textwidth]{P12_0hrs_apical.jpg} 	\end{tabular}\\
	\caption[Organ of Corti explants dissected at postnatal day 5, 7 and 12]{Organ of Corti explants in mouse cochleae (apical turn) from postnatal days 5 (A), 7 (B) and 12 (C) were dissected and fixed immediately to assess any dissection related damage. Cochleae from postnatal day 5 and 7 are well preserved with a minimal damage to the sensory epithelium. Organ of Corti from P12 cochlea was torn across the sensory epithelium, consistent with dissection damage. hc: Hensen's cells; HC: hair cells; KO: K\"{o}lliker's organ; SLM: spiral limbus; SL: spiral ligament. Scale bars = 30 $\mu$m.}%
	\label{fig:img_0hrs_control}%
\end{figure}

\begin{figure}
	\centering
	\textbf{Missing sensory epithelium}\\ [0.3cm]
	\begin{tabular}{cc}
		\includegraphics[width=0.4 \textwidth]{P12_missing_epithelium_10x} &  \includegraphics[width=0.3 \textwidth]{P12_no_epithelium} 
	\end{tabular}
	\caption[Organ of Corti explant with missing sensory epithelium (P12)]{The organ of Corti explant with missing sensory epithelium (P12). (A, B) An example of a cochlea that had the entire sensory epithelium separated from the spiral limbus during dissection. Scale bar = 100$\mu$m.}
	\label{fig:img_missing_epithelium}
\end{figure}

\subsection{Integrity of neonatal Organ of Corti explants in culture conditions}
Due to the extensive dissection-inflicted damage observed in the P12 cochlea, this age was deemed to be unsuitable for studying hair cell survival in ototoxic conditions. Hence, the hair cell survival in tissue culture conditions was studied in the P3 and P7 cochlea. In these preliminary studies, organ of Corti explants dissected from P3 and P7 cochleae were incubated in normal culture medium for a total of 42 hours and the sensory hair cell integrity was assessed qualitatively. Following 42 hour in culture, both P3 and P7 cochlea in the apical turn retained their organised structure and showed minimal hair cell loss (Fig. \ref{fig:img_42hrs_control} A and C). In the basal turn, the P3 cochlea showed excellent preservation of hair cell structures, demonstrating 1 row of IHC and 3 rows of OHC (Fig. \ref{fig:img_42hrs_control} B), but in the P7 cochlea, hair cells were in disarray with missing and structurally damaged inner and outer hair cells (Fig. \ref{fig:img_42hrs_control} D). \\[-1.5em]

\begin{figure}
	\centering
	\textbf{Neonatal cochlea in culture}\\[0.3cm]
	\begin{tabular}{ccc}
		& \textbf{Apical} &  \textbf{Basal}\\
		\begin{sideways}  \textbf{P3}\end{sideways} & \includegraphics[width=0.4\textwidth]{P3_42hrs_apical.jpg} & \includegraphics[width=0.4\textwidth]{P3_42hrs_basal.jpg} \\
		\begin{sideways}  \textbf{P7}\end{sideways} & \includegraphics[width=0.4\textwidth]{P7_42hrs_apical.jpg} & \includegraphics[width=0.4\textwidth]{P7_42hrs_basal.jpg} 
	\end{tabular}\\
	\caption[Organ of Corti explants at P3 and P7 after 42 hr incubation in culture medium]{Organ of Corti explants at P3 and P7 after 42 hr incubation in culture medium. Hair cell survival was excellent in the apical turns at both ages (A, C), and in the basal turn at P3 (B). However, the number of missing hair cells increased in the basal turn at P7 and the morphology of the organ of Corti deteriorated. Scale bar = 30 $\mu$m . }
	\label{fig:img_42hrs_control}%
\end{figure}

\subsection{Optimising neomycin concentration and exposure}
Neomycin is an aminoglycoside antibiotic toxic to the inner ear, which specifically affects the cochlear hair cells. In order to study the effect of extracellular nucleotides on hair cell survival after exposure to neomycin, the aimed was to generate approx. 50\% loss of hair cells so a change in either direction can be detected.\\[-1em]

A pilot study investigated different concentrations and incubation times for neomycin (0.5 mM for 6 and 12 hours; 1 mM for 3 hours, Table \ref{tab:neo_loss}).  P3 and P7 cochleae were pre-incubated in normal culture medium for 20 hours prior to neomycin exposure and fixed immediately at the end of the exposure. Total hair cell loss was approximately 8.5\% after a 6 hour exposure and approximately 27\% after 12 hours exposure. In both cases, the cell loss was less than the desired 50\% and variability between the cochleae was observed. When neomycin concentration was raised to 1 mM (6 hrs) total hair cell loss increased to 40\%, but the variability also increased. However, upon introducing a post-incubation period of 20 hours in normal culture medium following neomycin exposure (3 hrs), the total hair cell loss remained approximately the same (44\%). This was close to the target injury and the total hair cell loss became more consistent (Table. \ref{tab:neo_loss}). Therefore, all future studies were carried out using P3 cochleae with neomycin incubation at 1 mM for 3 hours and a post-incubation of 20 hours. \\[-1.5em]

\begin{table}
	\centering
	\onehalfspacing
	\textbf{\large Neomycin concentration and exposure} \\ [0.2cm]
	{\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{ccccc}
		& 20 hrs Normal Medium & IHC and OHC Loss (\%)\\
		\hline 
		\hline 
		\textbf{(P7)} &&\\
		& + 6 hrs 0.5mM (Neo)  & 8.50 $\pm$ 2.08 \\
		& + 12 hrs 0.5mM (Neo) & 27.00 $\pm$ 4.10 \\
		& + 6 hrs 1mM (Neo)	 & 40.30 $\pm$ 8.50 \\
		\textbf{(P3)} &&&&\\
		& + 3 hrs 1mM (Neo) + 20 hrs Medium    & 44.40 $\pm$ 0.40 \\
	\end{tabular}
	}
	\caption[IHC and OHC loss following neomycin exposure]{Cochlear explants were pre-incubated in normal culture medium for 20 hours and the neomycin was added at concentrations shown above. The loss of IHC and OHC is shown as percentage of the total number of inner and outer hair cell $\pm$ S.E.M (n=2).} 	
	\label{tab:neo_loss}
\end{table}

\pagebreak
\section{Discussion}
This chapter describes the development of a cell culture model for the subsequent studies investigating the role of extracellular nucleotides/nucleosides in hair cell survival after exposure to ototoxic aminoglycoside antibiotic neomycin. The objective was to develop an organotypic culture of the organ of Corti and optimise neomycin concentration and exposure time that would induce approximately 50\% hair cell loss. \\[-1em]

Dissection was performed on P3, P5, P7 and P12 -- 14 cochleae using a modified dissection technique and organotypic culture methods described previously \citep{parker2010primary,sobkowicz1993tissue}. Due to the calcification of the otic capsule at P12 -- 14, separating out an intact sensory epithelium was time consuming and difficult, resulting in tissue damage particularly at the basal turn. Therefore, the pilot studies focused mostly on P3 -- P7 cochleae. P3 and P7 cochleae have their differences, nevertheless dissection of P3 -- P7 cochleae was less challenging and could be achieved within a reasonable time frame (10 -- 15 minutes per cochlea). Performing the dissection within this time frame is crucial as it minimises the stress on tissues which could affect their survival in organotypic culture. Due to the vulnerability of P12 cochlea to dissection damage, only P7 and P3 were tested for their suitability for cell culture studies. Excellent sensory hair cell survival was observed in the apical turns of P3 and P7 explants after 42 hours in normal culture medium, but a greater number of missing hair cell and structural damage to remaining hair cells were observed in the basal turn of the P7 explants (Fig. \ref{fig:img_42hrs_control}). Although it was preferable to use a more mature cochlea for better comparison with the adult tissue, P3 was chosen due to better consistency in hair cell survival. The optimal concentration of neomycin and duration of exposure was also determined to achieve approximately 50\% hair cell loss. The optimal concentration of neomycin was 1 mM for 3 hours flanked by 20 hours of pre- and post-incubation in normal culture medium (Table \ref{tab:neo_loss}). \\[-1em]

The dissection and maintenance of tissue culture of the organ of Corti was described in detail by \citet{sobkowicz1993tissue}, which allowed the study of sensory cell responses to environmental stimuli. However, a drawback of this method is the technically demanding micro-dissection and the difficulty in maintaining a mature cochlea for an extended period of time. Although the tissue culture method retains a number of supporting structures, the dissection still disrupts the organ of Corti from its polarized fluid environment. Only culturing the whole cochlea would preserve the three dimensional structure and unique fluid composition where the apical side of sensory hair cells bathe in potassium-rich endolymph and the basolateral region in the perilymph. A novel technique for culturing whole cochlea using a rotating bioreactor to simulate microgravity was described by \citet{hahn2008whole}. Small openings were made on the anterior, lateral and the posterior aspect of the cochlea before being inserted into a High Aspect Ratio Vessel (HARV) containing culture medium rotating at 25 -- 35 rounds per minute (RPM). The HARV was kept in the incubator at standard cell culture condition (37$^\circ$C and 5\% CO$_2$). This method preserved the sensory structures and considerably reduced the lengthy and complicated micro-dissection required for tissue culture. In addition, hair cell survival was also improved to the extent that a P7 cochlea was morphologically intact after 7 days in culture \citep{hahn2008whole}. This is in contrast to significant hair cell degeneration observed after 5 days using standard culture method \citep{hahn2008whole}. The quiescent, low stress and low turbulence environment provided by the HARV simulated the microgravity environment and enhanced the hair cell survival. This methodology raises the exciting possibility of culturing a mature cochlea, but unfortunately this technique has not been developed further \citep{arnold2009novel}. \\[-1.5em]

\section{Summary and Conclusions}
In summary, this chapter outlined the issues related to the dissection of the neonatal organ of Corti and the survival of sensory hair cells in organotypic culture. The P3 cochlea was adopted for this study due to the simplicity of dissection and excellent survival in culture conditions. Neomycin concentrations and exposure times were assessed in both P3 and P7 cochleae. Exposure to 0.5 mM of neomycin (6 and 12 hours) resulted in a cell loss slightly below the 50\% target. Increasing the neomycin concentration to 1 mM (6 hours) achieved the target cell loss, but with a considerable amount of variability. Only the introduction of a 20 hour post-incubation period following neomycin treatment led to consistent target cell loss. Therefore, the final protocol established for the subsequent studies included a pre-incubation for 20 hours in normal culture medium, followed by exposure to neomycin (1 mM) for 3 hours and the final incubation in normal culture medium for 20 hours. 








