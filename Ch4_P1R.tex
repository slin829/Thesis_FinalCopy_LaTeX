\chapter{The Effect of P1 (Adenosine) Receptor Stimulation on Neomycin Ototoxicity}
\label{Ch4_P1R}
\addcontentsline{lof}{chapter}{Chapter \ref{Ch4_P1R}: The Effect of P1 (Adenosine) Receptor Stimulation on Neomycin Ototoxicity}
\addcontentsline{lot}{chapter}{Chapter \ref{Ch4_P1R}: The Effect of P1 (Adenosine) Receptor Stimulation on Neomycin Ototoxicity}
\section{Introduction}
Purinergic P1 and adenosine receptors (AR) are used interchangeably to describe the family of G protein-coupled receptors activated by adenosine. There are four subtypes of adenosine receptors (A\textsubscript{1}, A\textsubscript{2A}, A\textsubscript{2B}, A\textsubscript{3}) and each of these is coupled to either G\textsubscript{i} (A\textsubscript{1} and A\textsubscript{3}) or G\textsubscript{s} protein (A\textsubscript{2A},  A\textsubscript{2B}), which inhibit or activate adenylyl cyclase respectively. Activation of P1 (adenosine) receptors in the cochlea modulates cochlear blood flow and auditory neurotransmission, stimulates antioxidant defences, and limits inflammatory responses \citep{vlajkovic2009adenosine}. The extensive receptor-specific distribution of these receptors in the rat cochlea suggests a complex role for adenosine in regulation of cochlear function and response to stress, \citep{vlajkovic2007differential}. The adenosine A\textsubscript{1}R is of particular interest in cochlear protection, as the activation of this receptor reduces oxidative stress, limits glutamate excitotoxicity, and inhibits voltage-gated Ca\textsuperscript{2+} channels, which can prevent downstream activation of apoptotic and necrotic cell death pathway \citep{vlajkovic2014adenosine,vlajkovic2009adenosine}. The protective effects of the A\textsubscript{1}R agonists has been demonstrated after acoustic trauma \citep{wong2015mechanisms,vlajkovic2014adenosine} and in cisplatin ototoxicity \citep{whitworth2004protection,gunewardene2013adenosine}. In this study, the immunolocalisation of adenosine A\textsubscript{1}R in the developing mouse cochlea was described and the otoprotective effect of a selective A\textsubscript{1}R agonist ADAC in organotypic cultures of the organ of Corti was established.
\section{Methodology}
\subsection{Organ of Corti Tissue Culture}
To test the effect of P1 receptor agonists on neomycin ototoxicity, each agonist of interest was added to the normal culture medium and incubated for a total of 42 hours. Neomycin was introduced in the middle of the culture timeframe at 1 mM for 3 hours. For detailed description of dissection and cell culture techniques, refer to the previous chapters (Chapter \ref{Ch1_model}). The P1R agonists used in this study were adenosine, which activates all adenosine receptors, and adenosine amine congener (ADAC), that selectively activates A\textsubscript{1} adenosine receptor (A\textsubscript{1}R). The concentrations of adenosine and ADAC are shown in Table \ref{tab:Ch4_P1_agonists}. These concentrations were based on assumption that adenosine is quickly removed from the extracellular space by nucleoside transporters, whilst ADAC will remain in the extracellular space. All cochlear explants in this study were subjected to phalloidin labelling, epifluorescence imaging and cell quantification, followed by statistical analysis described in Chapter \ref{Ch2_neo}.\\[-1em]

%\begin{figure}
%	\centering
%	\includegraphics[width=0.9\textwidth]{culture_timeline}\\	
%	\caption[Cell culture timeline]{Timeline for tissue culture experiments. Total culture time was 42 hours.}%
%	\label{fig:Ch4_diag_culture_timeline}%
%\end{figure}
\begin{table}[h]
	\centering
	\onehalfspacing
	\caption[P1 receptor agonists and their concentrations]{P1 receptor agonists used in this study and their concentrations.}	
	{\renewcommand{\arraystretch}{1.2}
		\begin{tabular}{|l|ccc|}
			\hline
			\textbf{Agonists}& \textbf{P1 Receptors}   & \textbf{Concentration}   & \textbf{Supplier} \\
			\hline 
			\hline 
			Adenosine	 & A\textsubscript{1}, A\textsubscript{2A}, A\textsubscript{2B},  A\textsubscript{3}   & 100$\mu$M 	& Sigma Aldrich \\
			ADAC		 & A\textsubscript{1}    & 1$\mu$M    & Sigma Aldrich\\
			\hline
		\end{tabular}
	}
	\label{tab:Ch4_P1_agonists}
\end{table}
\subsection{Adenosine A\textsubscript{1} Distribution in the Cochlea }
\subsubsection{Tissue processing}
To study the distribution of adenosine A\textsubscript{1} receptor in the mouse (C57BL/6) cochlea, cochlear cross sections and surface preparations were used. These techniques are briefly described below. 
\paragraph{Cochlear cryosections} \mbox{}\\
P3 cochleae were extracted from the temporal bones using the protocol described in Chapter \ref{Ch1_model} . The cochleae were superfused with 4\% PFA through the round window and fixed in PFA overnight (4$^\circ$C). On the following day, the cochleae were washed with PBS and decalcified in Shandon TBD-1 Decalcifier (Thermo Fisher) for 30 minutes. Following wash in PBS, the cochleae were cryoprotected overnight with 30\% sucrose in 0.1M PBS at 4$^\circ$C. The cochleae were then mounted in OCT, snap-frozen with n-pentane and stored at -80$^\circ$C. Cochleae were cryosectioned at 30$\mu$M and immunostained with A\textsubscript{1}R-specific antibody using a floating section technique.
\paragraph{Cochlear surface preparation} \mbox{}\\
Following decapsulation and micro-dissection of the organ of Corti, the cochlear explants were incubated in normal culture medium or in culture medium containing neomycin. At the end of the 42 hour time frame, the cochlear explants were fixed with 4\% PFA for 30 mins at RT, followed by wash in 0.1M PBS (3 x 10 min).
\subsubsection{Immunohistochemistry and imaging}
Cochlear cryosections and surface preparations were permeabilised (1\% Triton X-100 in 0.1M PBS) and blocked (10\% normal donkey serum, NDS) for 1 hour at RT, followed by incubation with primary A\textsubscript{1}R antibody (rabbit anti-mouse, Alomone Labs, Jerusalem, Israel, 1:100 dilution) overnight at 4$^\circ$C. In control tissues, the primary antibody was omitted or pre-absorbed with the blocking peptide (1:1, Alomone Labs, Jerusalem, Israel). On the second day, the wholemounts and cryosections were washed three times with 0.1M PBS and incubated with the secondary antibody (Alexa-488 donkey anti-rabbit, 1:400) for 2 hours at RT. Cochlear tissues were then mounted in OCT for imaging.\\[-1em]

The surface preparations of the organ of Corti were double labelled with Alexa-647 Phalloidin (1:600 dilution) to visualise sensory hair cells, for 1 hour at RT, and mounted onto a microscope slide.\\[-1em]

The A\textsubscript{1}R immunofluorescence and phalloidin labelling were imaged simultaneously under a laser scanning confocal microscope (FluoViewTM FV1000, Olympus) at different wavelengths (635nm for A\textsubscript{1}R and 473nm for phalloidin) and processed with Olympus FluoView ver.1.7 software. For the surface preparation, a series of six to eight optical sections (Z-stacks) were collected from the tip of stereocilia at 2 $\mu$m per section.\\[-1em]

The images were obtained from two animals followed by qualitative image analysis.

\section{Results}
\subsection{Effect of A\textsubscript{1}R Stimulation on Cochlear Hair Cell Survival}
\subsubsection{Activation of adenosine receptors mitigates neomycin-induced hair cell loss}
In control explants (without neomycin), adenosine (100 $\mu$M, broadly selective P1 receptor agonist) or ADAC (1 $\mu$M, a selective adenosine A\textsubscript{1}R agonist) supplementation to the culture medium showed similar pattern of hair survival as the control explants incubated in normal culture medium (Fig. \ref{fig:Ch4:img_p1} A, C and E; Fig. \ref{fig:Ch4:cochleogram_ado} and Fig. \ref{fig:Ch4:cochleogram_adac40-60}).\\[-1em]

In neomycin-treated cochlear tissues, an improvement of hair cell survival was observed when adenosine or ADAC were added to the culture medium (Fig. \ref{fig:Ch4:img_p1} B, D and F). This protective effect of adenosine and ADAC was, however, restricted to the middle segment of the cochlea where the hair cells are most sensitive to neomycin ototoxicity. Adenosine supplementation significantly increased IHC survival in the middle cochlear segment (from 47.1\% $\pm$ 8.8\% in neomycin alone to 75.6\% $\pm$ 6.0\% in neomycin with adenosine, n = 6, p \textless 0.05, one-way ANOVA) and OHC survival (from 36.0\% $\pm$ 6.0\% in neomycin only to 57.2\% $\pm$ 6.3\% in neomycin with adenosine, n = 6, p \textless 0.01, one-way ANOVA, Fig. \ref{fig:Ch4:cochleogram_ado}). Similarly, ADAC supplementation to the culture medium also improved IHC and OHC survival in neomycin-treated tissues in the middle segment of the cochlea (to 89.4\% $\pm$ 3.3\% and 80.21\% $\pm$ 6.6\% respectively, p \textless 0.001, Fig. \ref{fig:Ch4:cochleogram_adac40-60}). Compared to adenosine, ADAC conferred a significantly greater otoprotective effect. In IHC, cochlear explants incubated with ADAC showed an additional 18\% increase in survival in comparison with explants incubated with adenosine (\textsl{p} \textless 0.05). Similarly, a 40\% increase was observed in OHC survival (\textsl{p} \textless 0.001). However, this otoprotective effect of ADAC on IHC appeared to be restricted to a narrower segment of the middle turn, whereas the otoprotective effect on OHC was broader (black lines in Fig. \ref{fig:Ch4:cochleogram_adac40-60}). In the apical and the basal segments, where neomycin did not induce a significant loss of hair cells, adenosine and ADAC supplementation did not affect hair cell survival (Figs. \ref{fig:Ch4:cochleogram_ado} and \ref{fig:Ch4:cochleogram_adac40-60}).\\[-1em]

\begin{figure}
	\centering
	\begin{tabular}{ccc}
		& \textbf{Control} & \textbf{Neo}\\
		\begin{sideways}  \textbf{Control}\end{sideways}  & \includegraphics[width=0.4 \textwidth]{control_m} & \includegraphics[width=0.4 \textwidth]{neo_m}\\ 
		\begin{sideways}  \textbf{Adenosine}\end{sideways}  & \includegraphics[width=0.4 \textwidth]{a_m} & \includegraphics[width=0.4 \textwidth]{aneo_m}\\
		\begin{sideways} \textbf{ADAC}\end{sideways} & \includegraphics[width=0.4 \textwidth]{adac_m} & \includegraphics[width=0.4 \textwidth]{adacneo_m}\\		
	\end{tabular}
	\caption[Cochlea wholemount treataed with adenosine or ADAC]{Cochlear explants incubated in (A) normal culture medium and normal culture medium enriched with (C) adenosine and (E) a selective adenosine A\textsubscript{1}R agonist (ADAC). (B) Neomycin induced substantial hair cell loss in normal culture medium was reduced by supplementation of (D) adenosine or (F) ADAC in the culture medium. IHC: Inner hair cells; OHC: Outer hair cells. Scale bars: main, 30 $\mu$m; insert, 10 $\mu$m.}
	\label{fig:Ch4:img_p1}
\end{figure}

\begin{figure}
	\centering
	\textbf{A. Adenosine (IHC)}\\
	\includegraphics[width=0.55\textheight]{cochleogram_Ado_IHC}\\[0.5cm]
	\textbf{B. Adenosine (OHC)}\\ 
	\includegraphics[width=0.55\textheight]{cochleogram_Ado_OHC}\\% 
	\caption[Cochleograms showing the effect of adenosine on hair cell survival]{Broadly selective P1R agonist adenosine significantly improved (A) IHC (\textsl{p} \textless 0.05) and (B) OHC (\textsl{p} \textless 0.01, One-way ANOVA) survival in the middle cochlear turn, most susceptible to neomycin ototoxicity. Data presented as mean $\pm$ S.E.M. (n= 5 in adenosine-treated explants and n =  6 in adenosine + neomycin explants).}
	\label{fig:Ch4:cochleogram_ado}
\end{figure}

\begin{figure}
	\centering
	\textbf{A. ADAC (IHC)}\\
	\includegraphics[width=0.55\textheight]{cochleogram_ADAC_IHC_middle40-60}\\[0.5cm]
	\textbf{B. ADAC (OHC)}\\
	\includegraphics[width=0.55\textheight]{cochleogram_ADAC_OHC}\\% 
	\caption[Cochleograms showing the effect of ADAC on hair cell survival]{Selective A\textsubscript{1}R agonist ADAC significantly (p \textless 0.001, One-way ANOVA) improved IHC and OHC survival in the middle turn of the cochlea (A, B). Data presented as mean $\pm$ S.E.M. (n= 4 in ADAC-treated explants and n = 6 in ADAC + neomycin explants).}
	\label{fig:Ch4:cochleogram_adac40-60}
\end{figure}
\subsection{Adenosine A\textsubscript{1}R Expression and Distribution}
\subsubsection{A\textsubscript{1}R distribution in the neonatal cochlea (P3)}
As the immunolocalisation of A\textsubscript{1}R has not been established in the developing cochlea, the distribution of A\textsubscript{1}R in the P3 mouse cochlea was investigated using cochlear cryosections and surface preparations of the sensory epithelium. This enabled the understanding of the differences in A\textsubscript{1}R distribution in the developing and adult mouse cochlea.\\[-1em]

Cross sections of the developing cochlea were immunolabelled with the adenosine A\textsubscript{1}R antibody and imaged under a laser scanning microscope (Fig. \ref{fig:Ch4:img_A1_P3_cryosection}). A\textsubscript{1}R immunolabelling was predominantly observed in cell bodies of the IHC and OHC, but also on the apical surface (reticular lamina) of the OHC in the basal turn (Fig. \ref{fig:Ch4:img_A1_P3_cryosection} B). The reticular lamina acts as a physical barrier separating cochlear fluids. Many sensory and supporting cell types contribute to the formation of this structure, including the cuticular plates of the OHC, the phalangeal processes of the supporting Deiters' cells and also Hensen's cells located lateral to the OHC. To identify the specific cell type labelled by A\textsubscript{1}R antibody, wholemount preparation was used to provide a surface view of the organ of Corti (Fig. \ref{fig:Ch4:img:A1_expression_Zstack_A-D}). The wholemount preparation from a P3 cochlea was labelled for A\textsubscript{1}R and actin filaments. The phalloidin labelling of actin filaments were localised to the actin-rich stereocilia and the cuticular plates of the hair cells (Fig. \ref{fig:Ch4:img:A1_expression_Zstack_A-D} A --C). The immunoexpression of A\textsubscript{1}R was confined to the cell bodies of the IHC (Fig. \ref{fig:Ch4:img:A1_expression_Zstack_A-D} B --E) and OHC (Fig. \ref{fig:Ch4:img:A1_expression_Zstack_A-D} D --E). The A\textsubscript{1}R immunolabelling in the reticular lamina was identified as the phalangeal processes of Deiters' cells that surround the OHC (Fig. \ref{fig:Ch4:img:A1_expression_Zstack_A-D} C). In addition, the pillar cells located in between the IHC and the first row of the OHC also showed A\textsubscript{1}R immunoexpression at the level of reticular lamina and below (Fig. \ref{fig:Ch4:img:A1_expression_Zstack_A-D} C --E). \\[-1em]

\begin{figure}
	\centering
	\begin{tabular}{ccc}
		& \textbf{Apical} & \textbf{Basal}\\
		\begin{sideways} \textbf{Untreated}\end{sideways} &
		\includegraphics[width=0.45\textwidth]{Ch4_A1_100_apical_OC_60x_C001Z005.png} & 
		\includegraphics[width=0.45\textwidth]{Ch4_A1_100_basal_OC_60x_C001Z003.png} \\
	\end{tabular}
	\begin{tabular}{ccc}
		& \textbf{No Primary control} & \textbf{Peptide Block}\\
		\begin{sideways} \textbf{}\end{sideways} &
		\includegraphics[width=0.45\textwidth]{Ch4_P3_A1_no1_basal_OC_40x} & 
		\includegraphics[width=0.45\textwidth]{Ch4_P3_A1_BP_apical_OC_40x} \\
	\end{tabular}
	\caption[Cross sections of the developing cochlea (P3) showing A\textsubscript{1}R immunolabelling]{Cross sections of the developing cochlea (P3) labelled with adenosine A\textsubscript{1}R antibody. A\textsubscript{1}R are immunoexpressed in both IHC and OHC. Strong immunolabelling was present at the apical surface of the OHC in the basal turn (arrowheads). IHC: Inner hair cells; OHC: Outer hair cells; bv: blood vessels. Scale bars: main, 30 $\mu$m; insert, 10 $\mu$m.} 
	\label{fig:Ch4:img_A1_P3_cryosection}
\end{figure}

\begin{figure}
	\centering 
	\begin{tabular}{ccc}
		& \textbf{Phalloidin} & \textbf{A$_1$R}\\
		\includegraphics[width=0.3\textwidth]{Ch4_OC_A} &
		\includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_pro_C002Z005} & \includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_green_C001Z005}\\
		\includegraphics[width=0.3\textwidth]{Ch4_OC_B} &
		\includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_pro_C002Z006} & \includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_green_C001Z006}\\
		\includegraphics[width=0.3\textwidth]{Ch4_OC_C} &
		\includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_pro_C002Z007} & \includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_green_C001Z007}\\
		\includegraphics[width=0.30\textwidth]{Ch4_OC_D} &
		\includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_pro_C002Z008} & \includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_green_C001Z008}\\		
	\end{tabular}	
	\caption[A\textsubscript{1}R expression in the organ of Corti at different focal planes (Cont.)]{Whole mounts of the organ of Corti showing A\textsubscript{1}R expression at different focal planes (Cont.)} 
	\label{fig:Ch4:img:A1_expression_Zstack_A-D}
\end{figure}
\begin{figure}[t]
	\ContinuedFloat 
	\centering 
	\begin{tabular}{ccc}
		\includegraphics[width=0.3\textwidth]{Ch4_OC_E} &
		\includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_pro_C002Z009} & \includegraphics[width=0.32\textwidth]{midbasal_control_40x_zstack_green_C001Z009}\\	
	\end{tabular}
	\caption[A\textsubscript{1}R expression in the organ of Corti at different focal planes]{Whole mounts of the organ of Corti showing A\textsubscript{1}R expression at different focal planes. Images were taken from the middle turn of a control cochlea and double labelled for adenosine A\textsubscript{1}R (green) and phalloidin (red). (A) at the level of stereocilia. (B) 2 $\mu$m below stereocilia. (C) 4 $\mu$m below stereocilia. Phalloidin labelling were restricted to the stereocilia and the cuticular plates of the hair cells. A\textsubscript{1}R immunolabelling was observed in Deiters� cell phalangeal processes, cell bodies of IHC, OHC and the pillar cells.  Schematics on the left represent the approximate focal planes of the surface preparation. IHC: Inner hair cells; OHC, Outer hair cells; dc: Deiters' cells; pc: pillar cells. Scale bars: main, 30 $\mu$m; insert, 10$\mu$m.} 
	\label{fig:Ch4:img:A1_expression_Zstack_E}
\end{figure}

Cochlear sections incubated without primary antibody demonstrated little autofluorescence (Fig. \ref{fig:Ch4:img_A1_P3_cryosection} C). However, antigen absorption study with blocking peptide showed staining in the endothelial wall of the blood vessels of the basilar membrane and within the stria vascularis (Fig. \ref{fig:Ch4:img_A1_P3_cryosection} D). This suggests that A\textsubscript{1}R labelling in these regions is non-specific (Fig. \ref{fig:Ch4:img_A1_P3_cryosection} A and B). However, thse staining within the lumen of the blood vessels could be immune cells such as monocytes and macrophages that are known to express A\textsubscript{1}R \citep{burnstock2014purinergicImmune}.\\[-1em]

In summary, A\textsubscript{1}R expression was detected in both the IHC and OHC of the developing cochlea. Supporting cells that expresses A\textsubscript{1}R include the Deiters' and the pillar cells. 

\pagebreak
\subsubsection{A\textsubscript{1}R expression after aminoglycoside treatment}
A\textsubscript{1}R expression after aminoglycoside treatment was assessed in the surface preparation of the developing cochlea. Neomycin exposed cochlea showed strong A\textsubscript{1}R expression in the middle turn Deiters cell processes, which corresponded to the region most sensitive to neomycin damage. (Fig. \ref{fig:Ch4:img:A1_doubleLabel} D). Pillar cell expression also appeared strongly in this region but was not observed in the apical and the basal turn.

\begin{figure}
	\centering
	\begin{tabular}{ccc}
		&\textbf{Control} & \textbf{Neo}\\
		\begin{sideways}  \textbf{Apical}\end{sideways} &	
		\includegraphics[width=0.38\textwidth]{Ch4_control_apical_05p} & 	\includegraphics[width=0.38\textwidth]{Ch4_neo_apical_5p}\\
		\begin{sideways}  \textbf{Middle}\end{sideways} &
		\includegraphics[width=0.38\textwidth]{Ch4_control_mid_50p} & \includegraphics[width=0.38\textwidth]{Ch4_neo_midbasal_75p}\\
		\begin{sideways}  \textbf{Basal}\end{sideways} &
		\includegraphics[width=0.38\textwidth]{Ch4_control_basal_90p}&
		\includegraphics[width=0.38\textwidth]{Ch4_neo_basal_90p} \\
	\end{tabular}
	\caption[Whole mounts of the organ of Corti double labelled with A\textsubscript{1}R and phalloidin]{Whole mounts of the organ of Corti in the developing cochlea (P3). Adenosine A\textsubscript{1}R immunostaining is shown in green, and phalloidin in red. The cochlear turns were defined in accordance with the previous chapters. (A, C, E) Control cochlea showing strongest A\textsubscript{1}R immunolabelling in the middle turn. (B, D, F) Neomycin-exposed cochlea demonstrated substantial loss of sensory hair cells in the middle turn and increased A\textsubscript{1} immunofluorescence in the supporting (scar) cells. A\textsubscript{1}R immunolabelling in supporting cells appears stronger than in control cochlea in all three segments. IHC: Inner hair cells; OHC: Outer hair cells. Arrowheads: Deiters' cells. Scale bars main: 30 $\mu$m; inset: 10 $\mu$m.} 
	\label{fig:Ch4:img:A1_doubleLabel}
\end{figure}
%  Apical turn, 0 -- 30\%; Middle turn, 35 -- 75\%; Basal turn, 80 -- 10\% of the cochlear length.
\section{Discussion}
In this study, supplementation of adenosine and ADAC in the culture medium significantly reduced ototoxicity in the middle segment of the cochlea, where the hair cells are most sensitive to neomycin toxicity. This suggests that the activation of adenosine receptors confers considerable protection for sensory hair cells exposed to neomycin. Neomycin-induced hair cell loss results from overproduction of ROS, followed by activation of the apoptotic pathways \citep{karasawa2011intracellular, matsui2004critical}. Neutralising the ROS with exogenous antioxidants and boosting endogenous antioxidant defences has been used previously as a treatment strategy to reduce aminoglycoside ototoxicity \citep{sha2000antioxidants,schacht1999antioxidant}. The stimulation of adenosine A\textsubscript{1}R also results in the stimulation of antioxidant defences and enhanced ROS scavenging. For example, administration of A\textsubscript{1}R agonist R-PIA induced increased activity of superoxide dismutase (SOD) and glutathione peroxidase in the chinchilla cochlea, which are the two principle antioxidant enzymes in the inner ear \citep{ford1997expression}. The A\textsubscript{1}R-mediated increase in endogenous antioxidant response likely underlies the protective effect of ADAC observed in this study. 

Additional AR subtypes are expressed in the sensory and supporting cells of the cochlea and includes the A\textsubscript{2A}R and A\textsubscript{3}R \citep{vlajkovic2007differential}. These receptors have broad physiological functions and are activated by adenosine \citep{jacobson2006adenosine}. Activation of A\textsubscript{3A}R agonist in rat leukocytes produces a similar increase in SOD and glutathione peroxidase activity, suggesting also an otoprotective roles of these receptors \citep{maggirwar1994adenosine}. A\textsubscript{2A}R are known to mediate increase in cochlear blood flow and stimulates anti-inflammatory responses \citep{maggirwar1994adenosine}. The protective role of A\textsubscript{2A}R has not been directly implicated in ototoxicity, but has been demonstrated in noise-induced hearing loss where the pathology involves the reduction in cochlear blood flow and post-exposure infiltration of inflammatory cells in the cochlea \citep{tan2015characterisation}. The activation of A\textsubscript{2B} and A\textsubscript{3} receptors have been linked to SAPK pathway, including JNK and p38, therefore suggesting a role of these receptors in apoptosis \citep{fredholm2001international}. In summary, adenosine as a broad P1R agonist activates all subtypes of AR and likely produces a mixed effect on cell survival depending on receptor expression. In contrast, the selective activation of A\textsubscript{1}R by ADAC appears to be critical for the survival of sensory hair cells exposed to neomycin.\\[-1em]

%The expression of adenosine receptors has been studied in the adult rat cochlea and 
A\textsubscript{1}, A\textsubscript{2A}, and A\textsubscript{3} receptors have all been localised to the IHC, SGN and the supporting Deiters' cells in adult Wistar rat cochlea \citep{vlajkovic2007differential}. In this study, the specific expression of A\textsubscript{1}R in the juvenile C57BL/6 mouse cochlea was investigated and a similar expression pattern was found in the IHC and the  Deiters' cells (head-plate of their phalangeal processes). In addition, A\textsubscript{1}R expression in the developing mouse cochlea was also detected in the OHC and the supporting pillar cells. Interestingly, the immunoexpression of A\textsubscript{1}R in the cochlea was strongest in the middle cochlear turn, which corresponds to the otoprotective effect of ADAC in the developing cochlea. This underlines the specificity of the ADAC effect.

\section{Chapter Summary}
In this study, a strong otoprotective effect of A\textsubscript{1} adenosine receptors on neomycin-induced hair cell loss was demonstrated. Neomycin-induced IHC and OHC loss was significantly reduced in the middle cochlear turn by the supplementation of AR agonists in the culture medium. Specific activation of adenosine A\textsubscript{1}R with ADAC resulted in a greater hair cell protection than broad activation of AR with adenosine. This suggests that the otoprotective effect of adenosine is likely mediated through the adenosine A\textsubscript{1}R. This is consistent with the previous studies from our lab \citep{vlajkovic2010adenosine,vlajkovic2014adenosine,vlajkovic2016adenosine}. \\[-1em]

The broad distribution of adenosine receptors in the juvenile mouse cochlea suggests a complex role of adenosine in cochlear response to stress. A\textsubscript{1}R immunoexpression in C57BL/6 mouse cochlea was immunolocalised to the IHC and OHC, and supporting pillar and Deiters' cells. The A\textsubscript{1} immunoexpression was also found to be turn-specific with strongest A\textsubscript{1}R immunolabelling in the middle cochlear turn corresponding to a strong otoprotective effect of adenosine and ADAC in this region. 
