\chapter{\textsl{In Vivo} Study}
\label{Ch5_invivo}
\addcontentsline{lof}{chapter}{Chapter \ref{Ch5_invivo}: \textsl{In Vivo} Study}
\addcontentsline{lot}{chapter}{Chapter \ref{Ch5_invivo}: \textsl{In Vivo} Study}
\section{Introduction}
In the previous chapter, selective activation of the adenosine A\textsubscript{1} receptor (A\textsubscript{1}R) was demonstrated to mitigate neomycin-induced ototoxicity in the organ of Corti explants of P3 mice. However, the applicability of these findings to the adult cochlea remains unclear. Another limitation of organotypic culture studies is the lack of a system that mimics the complexity of drug delivery and hence cannot replicate the uncertainty of systemic drug interactions and drug availability. Therefore, in this study, the effect of ADAC was investigated \textsl{in vivo} using adult C57BL/6 mice (6 - 8 weeks old). \\[-1em]

Mice are less susceptible to aminoglycoside antibiotics, and a high dose is required to induce ototoxicity \citep{blakley2008differences}. However, with most aminoglycosides, including neomycin, a single high dose required to induce ototoxicity is usually lethal. On the other hand, repeated injections of a lower dose are often insufficient to induce hair cell loss \citep{wu2001aminoglycoside}. For example, repeated intraperitoneal injections of kanamycin (900 mg/kg, once daily for 15 days) in mice resulted in significant threshold shift in the high frequencies, but it was ineffective in inducing hearing loss in the low frequencies \citep{hirose2011comparative}. When the kanamycin injections were followed closely by a daily single dose of a loop diuretic furosemide (50 mg/kg), the ototoxic effect extended to the lower frequencies \citep{hirose2011comparative}. While this regime of inducing aminoglycoside ototoxicity in adult animal models was promising, the need for repeated injections was perceived as a downside. It has been shown recently that a single high furosemide dose (100 - 400mg/kg) in combination with kanamycin could induce significant threshold shift (55 -- 85dB) and hair cell loss in adult mice \citep{versnel2007time,schmitz2014kanamycin}. This approach was therefore adopted in this study and ototoxicity was induced in mice using a single injection of kanamycin followed closely by furosemide (400 mg/kg).\\[-1em]

To test the effect of a selective A\textsubscript{1}R activation on aminoglycoside ototoxicity, ADAC was administered using intratympanic injection to reduce the likelihood of potential systemic side effects. Intratympanic injection (IT) is a commonly used clinical method for local drug delivery to the inner ear. It is a relatively simple procedure compared to methods such as the implantable pump systems and surgical insertion of biodegradable biopolymers \citep{salt2005local}. Drugs applied onto the round window membrane of the cochlea are expected to diffuse into the scala tympani and gradually distribute along the cochlear partition. The round window membrane is a three-layered structure, but it behaves as a semi-permeable membrane to a wide range of substances ranging in molecular weight and charge \citep{juhn1988review}. The ability of low molecular weight substances to diffuse across the round window membrane and its relatively simple administration makes IT injection a good choice for local drug delivery. \\[-1em]

The otoprotective effect of ADAC was postulated based on organotypic culture studies in the juvenile mouse cochlea. Although the A\textsubscript{1}R distribution was demonstrated in neonatal mouse organ of Corti (Chapter \ref{Ch4_P1R}), the immunoexpression of A\textsubscript{1}R has not been established in the adult C57BL/6 mouse. Previous studies in adult Wistar rats have demonstrated A\textsubscript{1}R immunoexpression in the inner hair cells, supporting Deiters' cells and spiral ganglion neurons. Due to a possibility of species-related differences in receptor distribution, the immunoexpression of A\textsubscript{1}R in adult C57BL/6 mice was also investigated and compared with the A\textsubscript{1}R distribution in the developing cochlea. 

\section{Methodology}
\subsection{\textsl{In Vivo} Study}
To study the effect of ADAC on aminoglycoside-induced ototoxicity, an IT injection of ADAC in poloxamer gel was administered 24 hours prior to an ototoxic procedure. Auditory brainstem responses (ABR) were measured prior to and 14 days after kanamycin/furosemide injections (Fig. \ref{fig:Ch5_timeline}).  \\[-1em]
\begin{figure}
	\centering
	\includegraphics[width=1 \textwidth]{Ch4_timeline}
	\caption[Timeline of \textsl{in vivo} experimental design]{Timeline (days) showing experimental design. Large empty circles: auditory function test or \textsl{in vivo} manipulation; closed circles: recovery days.} 
	\label{fig:Ch5_timeline}
\end{figure}
\subsubsection{Auditory brainstem responses}
Auditory brainstem responses (ABR) are short latency auditory evoked potentials derived from the activity of the auditory nerve and the brainstem structures that can be recorded using scalp electrodes. Adult C57BL/6 mice (6-8 weeks of age) were anaesthetised with intraperitoneal injections of ketamine (75 mg/kg) and domitor (0.5 mg/kg), and body temperature was maintained at 37$^\circ$C with a heating pad. Fine platinum electrodes were placed subdermally on the mastoid region of the test ear (left ear, reference electrode), scalp vertex (active electrode) and the mastoid region of the opposite ear (right ear, ground electrode). Auditory stimuli were presented to the left auditory canal through a 10 cm plastic tubing (closed field) at a rate of 21/s. Auditory stimuli were generated using the RZ6 auditory physiology workstation (Tucker-Davis Technologies (Alachua, FL, USA)) equipped with a computer-based sound processing software (BioSig, Alachua). A series of auditory clicks (10 $\mu$s square wave alternating in polarity) and tone pips (5 ms duration with 1 ms rise and fall time, 8 -- 28 kHz) were given at intensity ranging from 90 dB to 10 dB in 5 dB steps. The ABR complexes (wave I-V) were averaged (512 repeats with stimulus polarity alternated) and a low pass filter applied at 2000 Hz. The auditory threshold was determined at the lowest stimulus intensity (to the nearest 5 dB) where the ABR wave I was no longer distinguishable from the background noise. The ABR waveforms were repeated at each frequency and sound pressure level to determine the consistency of the responses.\\[-1em]

Auditory threshold shift (Final threshold - Baseline threshold) was presented as mean $\pm$ S.E.M for each group at each frequency. In the absence of normality (Shapiro-Wilk test), the data were analysed using non-parametric Kruskal-Wallis rank sum test followed by Dunn's multiple comparison with Benjamini and Hochlberg's correction. 

\subsubsection{Intratympanic injections}
\paragraph{Poloxamer gel} \mbox{}\\
Poloxamer-407 (F-127) is a pluronic gel characterised by low toxicity and reversible thermal gelation property \citep{gilbert1986drug}. It has been used as a medium for local drug delivery into the inner ear due to its slow release mechanism and gelation property at body temperature \citep{gilbert1986drug}. Vehicle and drug solutions were mixed with 17\% w/w poloxamer-407 powder, obtained from Sigma-Aldrich. The mixtures were placed on ice and left for approximately one hour to allow the poloxamer-407 powder to fully dissolve. To make 100 $\mu$M of ADAC in poloxamer gel, 100 $\mu$L of 1 mM ADAC was added to 1 mL of the control poloxamer gel in the liquid state. Once poloxamer-407 powder was fully dissolved, solutions were aliquoted in sterile Eppendorf tubes and frozen at -20$^\circ$C for later use. 
\paragraph{Drug delivery by intratympanic injection}\mbox{}\\
The animals were anesthetised using ketamine (75 mg/kg) and domitor (0.5 mg/kg). Local drug delivery to the round window membrane was achieved by intratympanic injection. Using a Hamilton syringe and a sterile needle, 5 $\mu$L of poloxamer-407 gel formulation, with or without ADAC, was injected into the middle ear through the tympanic membrane at the left superior quadrant as shown in Fig. \ref{diag:Ch5_tympanic_membrane_TI}. Care was taken to preserve the malleus in order to avoid conductive hearing loss. The animal was left to rest for 30 minutes on its side, allowing the poloxamer gel to set within the inner ear. Temgesic (buprenorphine) at 0.1 mg/kg was given for analgesia %prior to intratympanic injection. 
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{tympanic_membrane_TI}	
	\caption[Schematic diagram of the tympanic membrane indicating the site of injection]{A Schematic diagram of the tympanic membrane indicating the site of injection. ADAC was injected at the left superior quadrant of the tympanic membrane adjacent to the handle of the malleus (shown in orange).}%
	\label{diag:Ch5_tympanic_membrane_TI}%
\end{figure}
\subsubsection{Kanamycin and furosemide}
In this study, a combination of kanamycin and furosemide was used to induce an ototoxic effect in vivo, as neomycin is not well tolerated by mice and has a high mortality rate at the dose required to induce ototoxicity \citep{nord1967comparative}. Kanamycin also belongs to the class of aminoglycoside antibiotic and is predominantly cochleotoxic when co-administered with a loop diuretic (e.g. furosemide). Furosemide temporarily increases the permeability of the stria vascularis/endolymph barrier, thus increasing the uptake of kanamycin into the endolymph. This kanamycin/furosemide regime induces a permanent threshold shift (55 to 85 dB) in mice and is much better tolerated than neomycin \citep{versnel2007time}.  \\[-1em]

Mice were administered subcutaneously with 1000 mg/kg of kanamycin (Sigma Aldrich) in sterile saline, followed 45 minutes later by intraperitoneal injection of furosemide (400 mg/kg; Sigma Aldrich) in sterile saline. Due to the diuretic properties of furosemide, animals were given 0.9\% saline subcutaneously (0.1 ml/10g body weight) 24 hours after the furosemide injection. Three experimental groups were run simultaneously and is summarised in Table \ref{tab:Ch5_P2R_agonists}.\\[-1em]

\begin{table}[h!]
	\centering
	\onehalfspacing
	\caption[Summary of experimental manipulations]{Summary of IT and systemic injection in three experimental groups.}	
	{\renewcommand{\arraystretch}{1.2}
		\begin{tabular}{|l|cc|}
			\hline
			& \textbf{Intratympanic Injection}   & \textbf{Systemic Injection}  \\[-0.5em]
			& 									  & {\footnotesize (24hrs after IT injection)}  \\
			\hline 
			\hline 
			1. \textbf{Control}   	 & Saline  & Saline  \\
			2. \textbf{Kanamycin}		 & Saline    & Kanamycin/furosemide    \\
			3. \textbf{ADAC + kanamycin}  		 & ADAC (100 $\mu$M)     & Kanamycin/furosemide  \\
			\hline
		\end{tabular}
	}
	\label{tab:Ch5_P2R_agonists}
\end{table}
\subsubsection{Hair cell count}
\paragraph{Cochlear extraction and fixation} \mbox{}\\
Mice were euthanised by an overdose of pentobarbitone (90mg/kg body weight) immediately after the final ABR assessment. Cochleae were extracted using the protocol described previously, and superfused with 4\% PFA through the round window and fixed overnight in PFA at 4$^\circ$C. The cochleae were washed with PBS and decalcified in Shandon TBD-1 Decalcifier (Thermo Fisher) for 30 minutes on the following day. 
\paragraph{Whole mount tissue dissection} \mbox{}\\
Following extraction and decalcification, the cochlea was dissected using a protocol similar to the neonatal micro-dissection described previously with some modifications. The cochlea was decapsulated from the apical to the basal turn followed by the removal of the spiral ligament. The organ of Corti was then dissected into the apical segment, the basal segment and the hook region.   
\paragraph{Myosin VIIa labelling} \mbox{}\\
To label the hair cells, the wholemounts of the organ of Corti were immunostained with the myosin VIIa antibody. Microdissected cochlear tissue was permeabilised and blocked (1\% Triton X-100 and 10\% normal goat serum) for 1 hour at room temperature (RT) and incubated overnight at 4$^\circ$C with the primary antibody (rabbit anti-mouse Myosin VIIa, Proteus Biosciences Inc, Ramona, CA, 1:500 dilution). The cochlear wholemounts were then washed three times in PBS and incubated with the secondary antibody (Alexa-564 goat anti-rabbit; 1:500 dilution) for 2 hours at RT followed by wash in PBS (3 x 10 min). The tissues were then mounted onto a microscope slide using anti-fade mounting medium (Citiflour, Leicester, UK), coverslipped and sealed with nail polish as described previously. Hair cells were imaged under a laser scanning confocal microscope (FluoViewTM FV1000, Olympus) and processed with Olympus Fluoview ver.1.7 software. 
\subsection{A\textsubscript{1}R Distribution}
\subsubsection{A\textsubscript{1}R distribution in adult cochlea}
In this study, cross sections of the cochleae were labelled with adenosine A\textsubscript{1}R antibody to study receptor distribution. Cochleae were extracted, fixed overnight in 4\% PFA and decalcified in Shandon TBD-1 Decalcifier as described for myosin VIIa labelling. Following wash in PBS, the cochleae were cryoprotected overnight with 30\% of sucrose in 0.1M PBS at 4$^\circ$C. The cochleae were then mounted in OCT, snap-frozen with n-pentane and stored at -80$^\circ$C. \\[-1em]

Snap-frozen cochleae were cryosectioned at 30 $\mu$M, washed with 0.1M PBS permeablised (1\% Triton X-100 in 0.1M PBS) and blocked with 10\% normal donkey serum (NDS) for 1 hour at RT. This was followed by incubation with primary A\textsubscript{1} receptor antibody (rabbit anti-mouse, Alomone Labs, Jerusalem, Israel, 1:100 dilution) overnight at 4$^\circ$C. In control reactions, primary antibody was omitted or pre-absorbed with the blocking peptide (1:1, Alomone Labs, Jerusalem, Israel). On the second day, cochlear cryosections were washed three times with 0.1M PBS and incubated with the secondary antibody (Alexa-488 donkey anti-rabbit, 1:400) for 2 hours at RT. The cryosections were then mounted and imaged using a laser scanning confocal microscope (FluoViewTM FV1000, Olympus).\\[-1em]

The images were obtained from two animals and a descriptive analysis is provided.

\section{Results}
\subsection{The effect of ADAC on kanamycin ototoxicity in vivo }
\subsubsection{Intratympanic injection of ADAC does not ameliorate kanamycin-induced auditory threshold shifts}
Auditory ABR thresholds were assessed 2 days before and 14 days after intratympanic ADAC/vehicle injections. As expected, in kanamycin treated animals that received IT injection of saline, the threshold shifts for auditory clicks were elevated in comparison to the control mice that received IT and systemic injections of saline (55 $\pm$ 2 dB vs 10 $\pm$ 4 dB, \textsl{p} \textless 0.001, Kruskal- Wallis test, Fig. \ref{fig:graph:Ch5_ABR} A). Intratympanic injection of ADAC (100 $\mu$M) failed to reduce kanamycin-induced threshold shifts (60 $\pm$ 3dB, Fig. \ref{fig:graph:Ch5_ABR} A). Threshold shifts for tone pips showed a similar increase across the frequency range (8 -- 28 kHz) after kanamycin administration in vehicle-treated mice, and ADAC treatment was again ineffective in reducing kanamycin-induced threshold shifts (Fig. \ref{fig:graph:Ch5_ABR} B). 
\begin{figure}[p]
	\centering 
	\textbf{\Large\textbf{ABR}}
	\includegraphics[width=1 \textwidth]{Ch4_ABR} \\[0.5cm]
	\caption[Auditory brainstem responses showing the effect of ADAC intratympanic injections]{Auditory brainstem responses showing the effect of ADAC intratympanic (IT) injections on kanamycin-induced threshold shifts in C57BL/6 mice (8 weeks).  IT injection of either vehicle (saline) or ADAC (100 $\mu$M) were performed 24 hours prior to kanamycin and furosemide administration. In the control group (vehicle + saline) saline was administered instead of kanamycin and furosemide. Data presented as mean $\pm$ S.E.M (n= 10 -- 12). *** \textsl{p}  \textless 0.001 versus vehicle + saline, Kruskal-Wallis test with Dunn's multiple comparison.} 
	\label{fig:graph:Ch5_ABR}
\end{figure}
\begin{figure}[p]
	\centering 
	%\textbf{\huge \textsf{Wholemount}}\\[0.5cm]
	\begin{tabular}{ccc}
		Vehicle + Saline &  Vehicle + Kanamycin & ADAC + kanamycin   \\
		%\begin{sideways}  \textbf{Myosin VIIa}\end{sideways} &	
		\includegraphics[width=0.3 \textwidth]{Ch4_Control_myosin} & 
		\includegraphics[width=0.3 \textwidth]{Ch4_Kan_myosin} & \includegraphics[width=0.3\textwidth]{Ch4_ADACkan_myosin} \\
	\end{tabular}
	\caption[Myosin VIIa labelling of hair cells in control and kanamycin exposed cochlea]{Myosin VIIa labelling of hair cells in (A) control (vehicle + saline) and (B) kanamycin-exposed cochlea (vehicle + kanamycin). (C) ADAC did not protect against kanamycin-induced hair cell loss (ADAC + kanamycin). IHC: Inner hair cells; OHC: Outer hair cells. Scale bars: 30$\mu$m.} 
	\label{fig:img:Ch5_wholemount_kanamycin}
\end{figure}
\subsubsection{Kanamycin selectively targets the OHC}
The organ of Corti from the control cochlea (vehicle + saline) taken from the middle segment showed excellent preservation with no loss of IHC and OHC (Fig. \ref{fig:img:Ch5_wholemount_kanamycin} A). In animals that received kanamycin and furosemide injections, a complete loss of OHC, but good preservation of IHC was observed (Fig. \ref{fig:img:Ch5_wholemount_kanamycin} B). ADAC treatment did not improve the survival of OHC (Fig. \ref{fig:img:Ch5_wholemount_kanamycin} C), suggesting that this selective A\textsubscript{1} receptor agonist is ineffective against kanamycin ototoxicity after intratympanic administration. \\[-1em]


\subsection{Adenosine A\textsubscript{1}R Expression and Distribution}
\subsubsection{A\textsubscript{1} receptor distribution in the adult cochlea}
Adenosine A\textsubscript{1} receptor distribution in the mature (8 weeks) mouse cochlea was different from the developing (P3) cochlea. Strong immunolabelling was detected only in IHC in both the apical and the basal segment (Fig. \ref{fig:Ch5:img_A1_adult_cryosection} A -- B). No A\textsubscript{1}R immunolabelling was found in the OHC or the supporting cells of the organ of Corti (Fig. \ref{fig:Ch5:img_A1_adult_cryosection}). A\textsubscript{1} receptor expression was also assessed in the mature cochlea after kanamycin treatment. The expression pattern was similar to the control cochlea, showing strong A\textsubscript{1}R immunostaining in the IHC only (Fig. \ref{fig:Ch5:img_A1_adult_cryosection} C -- D) In summary, A\textsubscript{1}R are expressed predominantly in the IHC of the mature cochlea and this expression was unaffected by kanamycin administration.\\[-1em]

\begin{figure}
	\centering
	\begin{tabular}{ccc}
		& \textbf{Apical} & \textbf{Basal}\\
		\begin{sideways} \textbf{Control}\end{sideways} &	\includegraphics[width=0.43\textwidth]{Ch4_Adult_control_A1_apical_OC_60x_C001Z002} & 
		\includegraphics[width=0.43\textwidth]{Ch4_Adult_control_A1_basal_OC_60x_C001Z001} \\
		\begin{sideways} \textbf{Kanamycin}\end{sideways} &	\includegraphics[width=0.43\textwidth]{Ch4_Adult_neo_A1_apical_OC_60x_C001Z001} & 
		\includegraphics[width=0.43\textwidth]{Ch4_Adult_neo_A1_basal_OC_60x_C001Z002} \\
	\end{tabular}
	\begin{tabular}{ccc}
		& \textbf{No Primary control} & \textbf{Peptide Block}\\
		\begin{sideways} \textbf{}\end{sideways} &	
		\includegraphics[width=0.40\textwidth]{Ch4_Adult_NoP_apical_OC_40x} & 
		\includegraphics[width=0.40\textwidth]{Ch4_Adult_control_BP_basal_OC_40x} \\
	\end{tabular}
	\caption[Cross section of the adult mouse cochlea showing A\textsubscript{1}R immunolabelling]{Cross section of the adult (8 weeks) mouse cochlea labelled with adenosine A\textsubscript{1} receptor antibody. A\textsubscript{1} receptors are expressed strongly in the IHC of the (A -- B) control cochlea and (C -- D) kanamycin exposed cochlea. (E -- F) Negative controls (primary antibody omitted or pre-absobed with a corresponding peptide). Scale bars, 30$\mu$m. Abbreviations: IHC, Inner hair cells; OHC, Outer hair cells.} 
	\label{fig:Ch5:img_A1_adult_cryosection}
\end{figure}
\section{Discussion}
\subsection{The Effect of ADAC on Kanamycin Ototoxicity \textsl{in vivo}}
%In neonatal cochlear explants, ADAC supplementation to the culture medium significantly reduced neomycin-induced hair cell loss. In adult mice, ADAC was injected intratympanically onto the round window membrane of the cochlea in the form of a slow-release gel. 
Contrary to the hypothesis, IT injection of ADAC failed to protect the cochlea from kanamycin-induced ototoxicity, as measured by auditory threshold shifts and hair cell loss. Possible reasons accounting for this lack of otoprotective effect \textsl{in vivo} include different properties of the aminoglycosides tested (neomycin vs kanamycin), limited diffusion of ADAC through the round window membrane after IT injection, inadequate ADAC dose or differential expression of the A\textsubscript{1} receptors in the inner ear of adult and juvenile (P3) animals.\\[-1em]

In this study, kanamycin was used as a model to study ototoxicity in adult C57BL/6 mice. Kanamycin and neomycin are both ototoxic aminoglycosides targeting the sensory hair cells in the cochlea. It is, however, difficult to compare the ototoxic profiles of these aminoglycoside, as neomycin is suitable for cell culture models, whereas kanamycin is better suited for in vivo studies due to the better tolerance and lower mortality rate in vivo \citep{wu2001aminoglycoside}. Using the combination of kanamycin and furosemide, a approximately 50 dB threshold shift was observed across all tested frequencies (8 -- 28 kHz). In addition, a complete loss of OHC, but IHC preservation was observed. These findings were consistent with previous reports, demonstrating higher IHC resistance to kanamycin/furosemide treatment \citep{hirose2011comparative, schmitz2014kanamycin}. Intratympanic injection of ADAC did not provide any protection against the kanamycin induced auditory threshold shift or hair cell loss. As discussed in the previous chapter, the putative otoprotective effects of ADAC likely arises from enhanced endogenous antioxidant mechanisms associated with the activation of A\textsubscript{1}R. The lack of otoprotection in this study may indicate inadequate access of ADAC to the cochlea. The bioavailability of ADAC in the inner ear can be restricted by the RWM permeability for large molecules (ADAC MW is 576) and its efficacy by differences in A\textsubscript{1} receptor distribution in the juvenile and adult cochlea.
\subsubsection{Physical barriers restricting ADAC access to the inner ear}
As opposed to organotypic cultures, where hair cells were directly exposed to ADAC, the diffusion of ADAC to cochlear fluids via RWM is more restricted \textsl{in vivo}. In this study, ADAC was expected to diffuse across the RWM and into the cochlear fluids after local delivery to the middle ear, and thus become accessible to the sensory hair cells. \\[-1em]

One of the limitations of this study is the lack of control for drug concentrations in cochlear fluids. The release of ADAC from the poloxamer gel and the ability of ADAC to cross the round widow membrane are both important factors in determining ADAC concentrations in cochlear fluids. Drug release from poloxamer gel between 22\% -- 33\% w/w has been demonstrated to be inversely proportional to the diffusion coefficient \citep{gilbert1986drug}, hence with a poloxamer gel at concentration 17\% w/w, ADAC release should not be a limiting factor in this study. The round window membrane behaves as a semi-permeable membrane allowing substances with a low molecular weight such as electrolytes, antibiotics, dyes and steroids to passively diffuse into the scala tympani \citep{juhn1988review}. ADAC, having a molecular weight of 576, is classified as a relatively large molecule, but is still expected to cross the RWM \citep{juhn1988review}. Once in the perilymph, ADAC distribution from the basal turn to the middle and apical turns is also limited by passive diffusion along the relatively stationary cochlear fluids. This creates a high basal to apical gradient of ADAC concentration within the cochlea. The tone pip frequencies used in this study (4 -- 28 kHz) correspond to the arbitrary apical/middle segment of the mouse cochlea \citep{viberg2004guide}, hence a restricted ADAC diffusion from the basal turn to other regions of the cochlea could partly explain the absence of ADAC's otoprotective effect in this study. 
\subsection{A\textsubscript{1}R Expression and Distribution in Adult Cochlea}
The A\textsubscript{1} receptor was previously immunolocalised to the IHC, spiral ganglion neurons and the supporting Deiters' cells in the adult Wistar rat cochlea \citep{vlajkovic2007differential}. In the present study, the A\textsubscript{1}R in the mature mouse cochlea was primarily localised to the IHC (strong immunolabelling) with no expression in the OHC or the supporting cells. This also differs from the developing mouse cochlea where A\textsubscript{1}R immunoexpression was detected in IHC, OHC and the phalangeal process of the supporting Deiters' cells. This suggests that the distribution of A\textsubscript{1}R is both age- and species-specific. \\[-1em]

Following kanamycin/furosemide treatment, predominantly OHC loss was observed in the mature cochlea, whilst the IHC were mostly intact. The strong immunoexpression of A\textsubscript{1}R in the IHC could explain their resistance to kanamycin. The source of adenosine in the extracellular space could be ATP released from damaged OHC and ATP hydrolysis to adenosine by ectonucleotidases \citep{gale2004mechanism,vlajkovic2009adenosine}. On the other hand, the absence of A\textsubscript{1}R in the OHC may explain the complete loss of these cells after exposure to ototoxic aminoglycoside. This is consistent with the observation that the otoprotective effect of ADAC in the developing cochlea was strongest in the middle cochlear segment coinciding with the strong A\textsubscript{1}R immunolabelling. This suggests that the expression of A\textsubscript{1}R is an important determinant of the cell resistance to aminoglycoside antibiotics, and also underlies the otoprotective effect of ADAC.

\section{Chapter Summary}
\textsl{In vivo} otoprotective effect of ADAC against kanamycin-induced ototoxicity was investigated in this study. In contrast to the strong otoprotection observed in cochlear explants of juvenile mice, ADAC provided no protection against kanamycin-induced ototoxicity \textsl{in vivo}. These discrepancies can be attributed to the method of drug delivery and differential A\textsubscript{1}R expression in developing and mature cochlea. Intratympanic injection of ADAC onto the RWM provided a simple and direct route of local drug administration to the inner ear. However, this method of drug delivery relied on the RWM permeability for ADAC to reach cochlear fluids. ADAC was expected to passively diffuse across the RWM to the basal segment of the cochlea, but the slow distribution of ADAC to the middle and apical turns may potentially limit the efficacy of ADAC in those regions. \\[-1em]

Immunohistochemistry demonstrated differential distribution of A\textsubscript{1} receptor in the developing and mature cochlea. Kanamycin/furosemide treatment in the adult cochlea selectively wiped out the OHC but spared the IHC, which corresponds to the distribution of A\textsubscript{1} receptors. Therefore, the discrepancy in the efficacy of ADAC in the developing and mature cochlea may have resulted from differential A\textsubscript{1}R expression. 
%While the protective effect of ADAC was demonstrated in both the IHC and OHC in the juvenile (P3) cochlea, this effect was not observed \textsl{in vivo}. 



